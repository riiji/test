import {Field, ID, InputType, ObjectType} from "type-graphql";
import {v4 as uuid4} from 'uuid';
import {ArrayMaxSize, MaxLength} from "class-validator";

@ObjectType()
export class Recipe {
    @Field(type => ID)
    id: string;

    @Field()
    title: string;

    @Field()
    description?: string;

    @Field()
    creationDate: number;

    @Field(type => [String])
    ingredients: string[];
}

@InputType()
export class CreateRecipeInput {
    @Field()
    @MaxLength(30)
    title: string;

    @Field({nullable: true})
    @MaxLength(300)
    description: string;

    @Field(type => [String])
    @ArrayMaxSize(30)
    ingredients: string[];

    getRecipe() : Recipe {
        return {
            id: uuid4(),
            title: this.title,
            ingredients: this.ingredients,
            description: this.description,
            creationDate: Date.now(),
        };
    }
}