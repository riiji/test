import {getCollection} from "./mongo";
import {CreateRecipeInput, Recipe} from "./types";
import {InsertOneResult} from "mongodb";

export class RecipeService {
    async findById(id: string): Promise<Recipe> {
        const recipeCollection = await getCollection('recipes');
        const recipe = await recipeCollection.findOne({id: id}) as Recipe;
        console.log(recipe);
        return recipe;
    }

    async createRecipe(createRecipe: CreateRecipeInput): Promise<Recipe> {
        const recipeCollection = await getCollection('recipes');
        const recipe = createRecipe.getRecipe();
        await recipeCollection.insertOne(recipe);
        return recipe;
    }
}