import "reflect-metadata";
import {buildSchema} from "type-graphql";
import {RecipeResolver} from "./resolvers";
import Koa from 'koa';
import mount from "koa-mount";
import graphqlHTTP from "koa-graphql";

const app = new Koa();

const start = async () => {
    const schema = await buildSchema({
        resolvers: [RecipeResolver],
    })

    app.use(mount('/graphql', graphqlHTTP({
        schema: schema,
        graphiql: true
    })));

    app.listen(3000);
}

start().then(() => {});




