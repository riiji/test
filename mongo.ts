import {Collection, Db, MongoClient} from 'mongodb';

let cachedDb;
const uri = process.env.MONGO_URL;

export const connectToDatabase = async () : Promise<Db> => {
    if(cachedDb) {
        return cachedDb;
    }

    const client = await MongoClient.connect(uri);
    cachedDb = client.db('test');
    return cachedDb;
}

export const getCollection = async (collectionName: string) : Promise<Collection> => {
    const db = await connectToDatabase();
    return db.collection(collectionName);
}
