import {Arg, Mutation, Query, Resolver} from "type-graphql";
import {CreateRecipeInput, Recipe} from "./types";
import {RecipeService} from "./services";

class RecipeNotFoundError implements Error {
    constructor(id: string) {
        
    }

    message: string;
    name: string;
}

@Resolver(Recipe)
export class RecipeResolver {
    constructor(private recipeService: RecipeService) {
        this.recipeService = new RecipeService();
    }

    @Query(returns => Recipe)
    async recipe(@Arg("id") id: string) {
        const recipe = await this.recipeService.findById(id);
        if(recipe === undefined) {
            throw new RecipeNotFoundError(id);
        }
        return recipe;
    }

    @Mutation(returns => Recipe)
    addRecipe(@Arg('newRecipeData') newRecipeData: CreateRecipeInput) : Promise<Recipe> {
        return this.recipeService.createRecipe(newRecipeData);
    }

}